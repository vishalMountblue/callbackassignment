const fs = require("fs");
const path = require("path");

const readLipsumFile = (path, callback) => {
  let fileData = "";

  fs.readFile(path, "utf-8", (err, data) => {
    if (err) {
      console.log(err);
      return;
    } else {
      fileData = data;
      return callback(data);
    }
  });
};

const writeContentUppercase = (
  data,
  uppercaseFileName,
  uppercaseFilePath,
  callback
) => {
  fs.writeFile(uppercaseFilePath, data.toUpperCase(), (err) => {
    if (err) {
      console.log(err);
    } else {
      return callback(uppercaseFileName);
    }
  });
};

const writeFileName = (path, fileName, callback) => {
  fs.writeFile(path, fileName, (err) => {
    if (err) {
      console.log(err);
    } else {
      return callback(fileName);
    }
  });
};

const splittingDataInLine = (uppercaseFilePath, callback) => {
  fs.readFile(uppercaseFilePath, "utf-8", (err, data) => {
    if (err) {
      console.log(err);
    } else {
      return callback(data.toLowerCase());
    }
  });
};

// --next part--

const writeContentAsLine = (data, sentenceFilePath, callback) => {
  let lowercaseData = data.toLowerCase();

  fs.writeFile(
    sentenceFilePath,
    JSON.stringify(lowercaseData.split(".")),
    (err, data) => {
      if (err) {
        console.log(err);
      } else {
        return callback();
      }
    }
  );
};

const appendFileName = (path, fileName, callback) => {
  fs.appendFile(path, fileName, (err) => {
    if (err) {
      console.log(err);
    } else {
      return callback();
    }
  });
};

const readLineSepratedFile = (sentenceFilePath, callback) => {
  fs.readFile(sentenceFilePath, "utf-8", (err, data) => {
    if (err) {
      console.log(err);
    } else {
      return callback(JSON.parse(data).sort());
    }

    //console.log("data=> ",JSON.parse(data))

    console.log("sort data-> ", JSON.parse(data).sort());
    writeSortedData(
      JSON.parse(data).sort(),
      appendSortedDataFileName,
      readAndDeleteFiles,
      deleteFiles
    );
  });
};

const writeSortedData = (sortedData, sortedFilePath, callback) => {
  fs.writeFile(sortedFilePath, sortedData, (err) => {
    if (err) {
      console.log(err);
    } else {
      return callback();
    }
  });
};

const appendSortedDataFileName = (path, fileName, callback) => {
  fs.appendFile(path, fileName, (err) => {
    if (err) {
      console.log(err);
    } else {
      return callback();
    }
  });
};

const readAndDeleteFiles = (filenamesPath, callback) => {
  fs.readFile(filenamesPath, "utf-8", (err, data) => {
    if (err) {
      console.log(err);
    } else {
      return callback(data);
    }
  });
};

const deleteFiles = (filenames) => {
  filenameArray = filenames.split("\n");

  for (file of filenameArray) {
    fs.unlink(path.join(__dirname, file), (err) => {
      if (err) {
        console.log(err);
      }
    });
  }
};

module.exports = {
  readLipsumFile,
  writeContentUppercase,
  writeContentAsLine,
  splittingDataInLine,
  writeFileName,
  appendFileName,
  readLineSepratedFile,
  writeSortedData,
  appendSortedDataFileName,
  readAndDeleteFiles,
  deleteFiles,
};
