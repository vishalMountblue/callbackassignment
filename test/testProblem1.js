const {  createDirectory, createFiles,deleteFiles } = require("../problem1");
const path = require('path')
const fs = require("fs");


let folderName = '../folder1'
let folderPath = path.join(__dirname,folderName)

createDirectory(folderPath,(path)=>{

    createFiles(path,(folderPath,numberOfFiles)=>{

        deleteFiles(folderPath,numberOfFiles)
    })
})
