const fs = require("fs");
const path = require("path");

const {
  readLipsumFile,
  writeContentUppercase,
  writeContentAsLine,
  splittingDataInLine,
  writeFileName,
  appendFileName,
  readLineSepratedFile,
  writeSortedData,
  appendSortedDataFileName,
  readAndDeleteFiles,
  deleteFiles,
} = require("../problem2");

const lipsumFilePath = path.join(__dirname, "../lipsum.txt");
readLipsumFile(lipsumFilePath, (data) => {
  const fileName = "newContentsFile.txt";
  const newFilePath = path.join(__dirname, "../newContentsFile.txt");

  writeContentUppercase(data, fileName, newFilePath, (uppercaseFileName) => {
    let filnamesFileName = "filenames.txt";
    let filenamesFilePath = path.join(__dirname, "../filenames.txt");

    writeFileName(filenamesFilePath, uppercaseFileName, (uppercaseFileName) => {
      let uppercaseFilePath = path.join(__dirname, `../${uppercaseFileName}`);
      splittingDataInLine(uppercaseFilePath, (lowercaseData) => {
        const sentenceFilePath = path.join(__dirname, "../sentenceFile.txt");

        writeContentAsLine(lowercaseData, sentenceFilePath, () => {
          const sentenceFileName = "\nsentenceFile.txt";
          const filenamesPath = path.join(__dirname, "../filenames.txt");

          appendFileName(filenamesPath, sentenceFileName, () => {
            const sentenceFilePath = path.join(
              __dirname,
              "../sentenceFile.txt"
            );
            readLineSepratedFile(sentenceFilePath, (sortedData) => {
              const sortedFilePath = path.join(
                __dirname,
                "../sortedDataFile.txt"
              );

              writeSortedData(sortedData, sortedFilePath, () => {
                const sortDataFileName = "\nsortedDataFile.txt";
                const fileNamesPath = path.join(__dirname, "../filenames.txt");
                appendSortedDataFileName(
                  fileNamesPath,
                  sortDataFileName,
                  () => {
                    const fileNamesPath = path.join(
                      __dirname,
                      "../filenames.txt"
                    );
                    readAndDeleteFiles(fileNamesPath, (filenamesData) => {
                      deleteFiles(filenamesData);
                    });
                  }
                );
              });
            });
          });
        });
      });
    });
  });
});
