const fs = require("fs");
const prompt = require("prompt-sync")();
const path = require("path");

const createDirectory = (path, callback) => {
  try {
    if (!fs.existsSync(path)) {
      fs.mkdirSync(path);
      return callback(path);
    } else {
      console.error("Directory already present");
    }
  } catch (err) {
    console.error(err);
  }
};

const createFiles = (folderPath, callback) => {
  let numberOfFiles = prompt(
    "Please enter number of files you want to create: "
  );
  const content = "This is a text file for test purpose";

  for (let filesCount = 0; filesCount < numberOfFiles; filesCount++) {
    const filePath = path.join(folderPath, `file${filesCount}.txt`);

    fs.writeFile(filePath, content, (err) => {
      if (err) {
        console.log(err);
      }
    });
  }
  return callback(folderPath, numberOfFiles);
};

const deleteFiles = (directoryPath, filesCount) => {
  console.log("dirpath ", directoryPath);
  for (let fileCounter = 0; fileCounter < filesCount; fileCounter++) {
    fs.unlink(`${directoryPath}/file${fileCounter}.txt`, (err) => {
      if (err) {
        console.log(err);
      }
    });
  }
};

module.exports = { createDirectory, createFiles, deleteFiles };
